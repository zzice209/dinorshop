import React from "react";
import {
    Switch,
} from "react-router-dom";
import RouterComponent from './RouterComponent'

const SwitchComponent = () => {
  return (
    <Switch>
        <RouterComponent />
    </Switch>
  );
};

export default SwitchComponent
