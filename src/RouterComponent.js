import React from "react";
import ROUTERSCONFIG from "./RouterConfig";
import {
    Route
} from "react-router-dom";


const RouterComponent = () => {
  return (
    <div>
      {ROUTERSCONFIG.map((router, index) => (
        <React.Fragment key={index}>
            <Route path={router.path} component={router.component} exact={router.exact}/>
        </React.Fragment>
      )
      )}
    </div>
  );
};

export default RouterComponent;
