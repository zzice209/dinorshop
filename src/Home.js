import React from 'react'
import AddTodo from "./components/AddTodo";
import TodoList from "./components/TodoList";
import VisibilityFilters from "./components/VisibilityFilters";
import "./styles.css";

const Home = () => {
    return (
        <div className="todo-apps">
            <h2>Todo List</h2>
            <AddTodo />
            <TodoList />
            <VisibilityFilters />
        </div>
    )
}

export default Home