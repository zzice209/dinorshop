import React from 'react'

const Home = React.lazy(() => import('./Home'))

const Topics = React.lazy(() => import('./Topics'));

const About = React.lazy(() => import('./About'));

const ROUTERSCONFIG = [
    {
        path: '/about',
        component: About,
        exact: false,
        name: 'About'
    },
    {
        path: '/topics',
        component: Topics,
        exact: false,
        name: 'Topics'
    },
    {
        path: '/',
        component: Home,
        exact: true,
        name: 'Home'
    }
]

export default ROUTERSCONFIG