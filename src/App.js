import React, { Suspense } from "react";
import { BrowserRouter as Router } from "react-router-dom";
import SwitchComponent from "./SwitchComponent";
import NavComponent from './NavComponent'
// import store from './redux/store'

function App() {
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <div>
          {/* <div>{store.getState()}</div> */}
          {/* <button onClick={e => store.dispatch({ type: "INCREMENT" })}>
            plus
          </button>
          <button onClick={e => store.dispatch({ type: "DECREMENT" })}>
            minus
          </button>
          <button onClick={e => store.dispatch({ type: "ZERO" })}>zero</button>
        </div>
        <button onClick={e => store.dispatch({ type: "INCREMENT" })}>
          Increment
        </button> */}
          <NavComponent></NavComponent>
          <SwitchComponent />
        </div>
      </Suspense>
    </Router>
  );
}

export default App;
