import React from 'react'
import RouterConfig from './RouterConfig'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
// import { increment, decrement, reset } from './actionCreators'

const NavComponent = (props) => {
    const { counter } = props
    return (
        <>
            <ul>
                {RouterConfig.map((router, index) => (
                    <li key={index}>
                        <Link to={router.path}>{router.name}{counter}</Link>
                    </li>
                ))}
            </ul>
        </>
    )
}

const mapStateToProps = (state /*, ownProps*/) => {
    return {
      counter: state.counter
    }
  }
  
//   const mapDispatchToProps = { increment, decrement, reset }

export default connect(mapStateToProps)(NavComponent)